# Mission Thrift Store Map

This tool was designed to extract thrift store locations from the official [Mission Thrift Store](https://missionthriftstore.com/store-listing) website and place them into KML format. 

## Dependencies
* Python 3.x
    * Requests module for making https requests
    * Beautiful Soup 4.x (bs4) for scraping
    * Geocoder for retrieving geographic coordinates for addresses from OpenStreetMap's Nominatim service 
    * Simplekml for easily building KML files
* Also of course depends on official [Mission Thrift Store](https://missionthriftstore.com/store-listing) website. 

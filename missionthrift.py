#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import geocoder
import simplekml

#Set filename/path for KML file output
kmlfile = "missionthrift.kml"
#Set KML schema name
kmlschemaname = "missionthrift"
#Set page URL
pageURL = "https://missionthriftstore.com/store-listing"

#Returns reusable BeautifulSoup of a given site
def getsoupinit(url=pageURL):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(url)
    #Return soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns a list of store names, addresses, and lat/lng for a given site url
def getstores(url=pageURL):
    #Initialize the soup
    soup = getsoupinit(url)
    #Initialize stores list
    stores=[]
    #Get the relevant div tags, identified by their 'store-data' class
    divs = soup.find_all('div', class_="store-data")
    #Loop through all the store-data divs
    for div in divs:
        #Get the name of the store by its link text 
        name = div.h2.a.get_text()
        #Filter down the address data to its collection of span tags
        addressdata = div.p.a.find_all('span')
        #Form a full address line from all the address-related span tags
        storeaddress = addressdata[0].get_text()+addressdata[1].get_text()+addressdata[2].get_text()+addressdata[3].get_text()
        #Run the geocode
        geo = geocoder.osm(storeaddress)
        #Skip over stores with failed geocodes - it would be nice to fix the geocodes without results, but the results are decent enough for now 
        if geo.ok == False:
            continue
        #Get the coordinates from the geocode
        lat = geo.y
        lng = geo.x
        #Append the 4 data points collected here into the array associated with this site
        stores.append([name,storeaddress,lat,lng])
    return stores

#Saves a KML file of a given list of stores
def createkml(stores):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through list for each store
    for store in stores:
        #Get the name of the store
        storename = str(store[0])
        #Get coordinates of the store
        lat = store[2]
        lng = store[3]
        #First, create the point name and description in the kml
        point = kml.newpoint(name=storename,description=str(store[1]))
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)
#Bring it all together
createkml(getstores())
